package com.jiushi6.officeonline.service.impl;

import com.jiushi6.officeonline.service.IFileService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Project: officeonline
 * @Package com.jiushi6.officeonline.service.impl
 * @Description: 本地存储文件实现类
 * @author: 飞翔的荷兰人
 * @date Date : 2021年01月07日 下午 21:24
 */
@Service
public class FileLocalServiceImpl implements IFileService {


    /**
     * 文件上传接口
     * @param file 文件
     * @return 操作结果
     */
    @Override
    public boolean upload(MultipartFile file) {

        return false;
    }

    /**
     * 文件 下载接口
     * @param fileId 文件ID
     * @param request 请求
     * @param response 响应
     */
    @Override
    public void download(String fileId, HttpServletRequest request, HttpServletResponse response) {

    }
}
