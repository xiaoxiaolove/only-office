package com.jiushi6.officeonline.controller;

import com.jiushi6.officeonline.bean.AjaxResult;
import com.jiushi6.officeonline.farmework.base.BaseController;
import com.jiushi6.officeonline.service.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Project: officeonline
 * @Package com.jiushi6.officeonline.controller
 * @Description: 文件控制层
 * @author: 飞翔的荷兰人
 * @date Date : 2021年01月07日 下午 21:05
 */
@Controller
@RequestMapping("file")
public class FileController extends BaseController {

    /**
     * 文件服务
     */
    @Autowired
    private IFileService fileService;
    /**
     * 此处给Onlyoffice回调保存接口
     * @param file
     * @return
     */
 @PostMapping(value = "/office/out/docx/save", produces = "application/json;charset=UTF-8")
    public void saveWord(@RequestParam Map<String, String> map, HttpServletRequest request, HttpServletResponse response) {
        logger.info("==========开始保存ONLYOFFICE文档==========");
        PrintWriter writer = null;
        String body = "";
        try {
            writer = response.getWriter();
            Scanner scanner = new Scanner(request.getInputStream());
            scanner.useDelimiter("\\A");
            body = scanner.hasNext() ? scanner.next() : "";
            scanner.close();
        } catch (Exception ex) {
            writer.write("get request.getInputStream error:" + ex.getMessage());
            return;
        }

        if (body.isEmpty()) {
            throw new CustomException("ONLYOFFICE回调保存请求体未空");
        }
        logger.info("ONLYOFFICE回调保存请求体是:[{}]", body);

        JSONObject jsonObj = JSON.parseObject(body);
        int status = (Integer) jsonObj.get("status");
        int saved = 0;
        String keyStr = jsonObj.get("key").toString();
        long key = Long.parseLong(keyStr.substring(keyStr.indexOf("_")+1));
        logger.info("原文件ID是:[{}]", key);
        if (status == 2 || status == 3 || status == 6) //MustSave, Corrupted
        {
            String downloadUri = (String) jsonObj.get("url");
            logger.info("ONLYOFFICE回调保存文件下载地址是:[{}]", downloadUri);
            try {
                URL url = new URL(downloadUri);
                java.net.HttpURLConnection connection = (java.net.HttpURLConnection) url.openConnection();
                InputStream is = connection.getInputStream();
                if (is == null) {
                    throw new CustomException("获取文件流失败!!");
                }

                try (ByteArrayOutputStream bao = new ByteArrayOutputStream()) {
                    //TODO 此处为覆盖原文件逻辑请自己更换
                    BusFiles busFiles = busFilesService.selectBusFilesById(key);
                    if (null == busFiles) {
                        throw new  CustomException("未获取到原文件信息");
                    }
                    logger.info("原文件信息[{}]", busFiles);
                    String fileName = busFiles.getFileName();
                    int read;
                    final byte[] bytes = new byte[1024];
                    while ((read = is.read(bytes)) != -1) {
                        bao.write(bytes, 0, read);
                    }
                    bao.flush();
                    byte[] bytes1 = bao.toByteArray();
                    if (null == bytes1 || bytes1.length < 0) {
                        throw new CustomException("获取下载文件流数据失败");
                    }
                    //TODO 此处为上传新文件进文件服务器请自行替换
                    String[] upload = FastDfsUtil.upload(bytes1);
                    String md5 = DigestUtils.md5DigestAsHex(bytes1);
                    busFiles.setMd5(md5);
                    busFiles.setGroupId(upload[0]);
                    busFiles.setFilePath(upload[1]);
                    busFilesService.updateBusFiles(busFiles);
                    logger.info("更新后的文件信息[{}]", busFiles);
                    //缓存key,缓存10s
                    redisCache.setCacheObject(keyStr,busFiles,10, TimeUnit.SECONDS);
                    System.out.println("缓存key:"+keyStr+",缓存value:"+ JSON.toJSONString(busFiles));
                }
                connection.disconnect();

            } catch (Exception ex) {
                saved = 1;
                ex.printStackTrace();
            }
        }
        writer.write("{\"error\":" + saved + "}");
    }

/**
     * 根据文件id下载文件 提供给ONLYOFFICE调用接口
     *
     * @param fileId   文件ID
     * @param request  请求
     * @param response 响应
     * @return 通过response返回文件信息
     */
    @GetMapping("downLoad/{fileId}")
    public void downLoad(@PathVariable("fileId") Long fileId, HttpServletRequest request, HttpServletResponse response) {
        //TODO 此处自己提供下载服务给ONLYOFFICE调用
        busFilesService.downLoad(fileId, request, response);
    }
}
